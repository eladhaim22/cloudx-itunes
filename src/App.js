import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route } from "react-router-dom";
import Main from './pages/Main.js';


class App extends Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (
      <BrowserRouter>
        <Route path="" component={Main} />
      </BrowserRouter>
    );
  }
}

export default App;
