import axiosClass from 'axios';
import { DISABLE_LOADER, ENABLE_LOADER } from '../actions/types';
import store from '../config/store';

const instance = axiosClass.create();

instance.interceptors.request.use(config => {
	enableLoader(store);
	return config;
}, undefined);

instance.interceptors.response.use(config => {
	disableLoader(store);
	return config;
}, undefined);

const enableLoader = (store) => {
    return store.dispatch({type:ENABLE_LOADER})
};

const disableLoader = (store) => {
    return store.dispatch({type:DISABLE_LOADER})
};

export let axios = instance;


