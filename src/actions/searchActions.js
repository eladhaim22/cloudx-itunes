import {axios} from './axiosWrapper';
import { SEARCH_SUCCESS, SEARCH_ERROR } from '../actions/types';

const limit = 200;

export const search = (searchTerm,searchType) => {
    return dispatch =>(  
        axios.get('/api/search?' + buildQueryString(searchTerm,searchType)).then(response =>
            dispatch({type:SEARCH_SUCCESS,payload:response.data})
        )
        .catch(error =>
            dispatch({type:SEARCH_ERROR,payload:error})
        ))
};

const buildQueryString = (searchTerm,searchType) => {
    let entity = searchType == 1 ? 'allArtist' : 'album'; 
    let attribute = searchType == 1 ? 'artistTerm' : 'albumTerm';
    let queryString = new URLSearchParams([["term", searchTerm],["entity", entity],["attribute",attribute],["limit",limit]]);
    return queryString;
}