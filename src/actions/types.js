export const SEARCH_SUCCESS = 'search_success',
             SEARCH_ERROR = 'search_error',
             ENABLE_LOADER = 'enable_loader',
             DISABLE_LOADER = 'disable_loader';