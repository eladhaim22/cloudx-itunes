import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import AudioTrackIcon from '@material-ui/icons/Audiotrack';
import {Link} from 'react-router-dom';


const SearchListItem = (props) => {
  const { data,listClass } = props;
  return (<Link to={`/album/${data.collectionId}`}><ListItem className={listClass}>
        <ListItemAvatar>
          <Avatar>
            <AudioTrackIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={data.collectionName}
          secondary={data.artistName}
        />
      </ListItem></Link>)
};

export default SearchListItem;