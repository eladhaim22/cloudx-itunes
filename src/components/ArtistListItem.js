import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import PersonIcon from '@material-ui/icons/Person';
import {Link} from 'react-router-dom';

const SearchListItem = (props) => {
  const { data,listClass } = props;
   return (<Link to={`/artist/${data.artistId}/albums`}><ListItem className={listClass}>
        <ListItemAvatar>
          <Avatar>
            <PersonIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={data.artistName}
        />
      </ListItem></Link>)
};

export default SearchListItem;