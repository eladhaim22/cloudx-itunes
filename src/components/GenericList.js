import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import TablePagination from '@material-ui/core/TablePagination';

const styles = theme => ({
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  listRoot:{
  	width:'50%',
  	display:'inline-block'
  },
  listClass:{
  	border: '2px solid lightgreen',
    borderRadius: '5px',
    marginBottom: '3px',
    background: 'white'
  },
  root:{
  	color:'white'
  },
  selectIcon:{
  	color:'white'
  }
 })

class GenericList extends Component {
  
	constructor(props) {
		super(props);
		this.state = {
		    rowsPerPage: 10,
		    rows: [],
		    page: 0
		};
	}

	componentWillMount(){
		this.handleChangePage(null,0);
	}


	componentDidUpdate(prevProps){
	  	if(this.props.items != prevProps.items){
	  		this.handleChangePage(null,0);
	  	}
  	}

	handleChangePage = (event, page) => {
		var startIndex = page * this.state.rowsPerPage;
		var endIndex = Math.min(startIndex + this.state.rowsPerPage - 1, this.props.items.length - 1);
		let rows = this.props.items.slice(startIndex, endIndex + 1);
		this.setState({ page:page,rows:rows });
	};

	handleChangeRowsPerPage = event => {
		this.setState({ rowsPerPage: event.target.value, page:0},function () {
			this.handleChangePage(null,0);
		});  
	};

	injectPropsToComponent(row){
		return React.cloneElement(this.props.itemComponent, {data: row,listClass: this.props.classes.listClass});
	}

	render() {
		const { classes } = this.props;
		return (
		<div>
			 <div className={classes.listRoot}>
			  <List dense={false} >
			    {this.state.rows.map(row => this.injectPropsToComponent(row))}
			  </List>
			  {this.state.rowsPerPage < this.props.items.length ? 
				  <TablePagination
				  	  className="table-pagination"
				      rowsPerPageOptions={[5, 10, 25]}
				      component="div"
				      count={this.props.items.length}
				      rowsPerPage={this.state.rowsPerPage}
				      page={this.state.page}
				      backIconButtonProps={{
				        'aria-label': 'Previous Page',
				      }}
				      nextIconButtonProps={{
				        'aria-label': 'Next Page',
				      }}
				      classes={{root:classes.root,selectIcon:classes.selectIcon}}
				      onChangePage={this.handleChangePage.bind(this)}
				      onChangeRowsPerPage={this.handleChangeRowsPerPage}
				  /> 
				: null}
			</div>
		</div>
	)}
}
export default withStyles(styles)(GenericList);