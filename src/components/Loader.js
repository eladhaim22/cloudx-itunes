import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';


const Loader = ({ containerStyle,imgStyle, size }) => (
    <div style={containerStyle ? containerStyle : { mixBlendMode:'multiply', zIndex:'1000',textAlign:'center',position: 'absolute',top:'50%',transform:'translate(-50%,-50%)',left: '50%' }} className="loader-container">
        <CircularProgress  color={'#005fab'} />
    </div>
);


export default Loader;