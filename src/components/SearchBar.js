import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const styles = {
  root: {
    padding: '0px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  input: {
    marginLeft: 8,
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4,
  },
};

class SearchBar extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      filter:"1",
      searchTerm: ''
    }    
  }

  handleChange = (type,event) => {
    this.setState({[type]:event.target.value});
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state.searchTerm,this.state.filter);
  }

  render(){
    const { classes } = this.props;
    return (
      <form className={this.props.formClass} onSubmit={this.handleSubmit.bind(this)}>
        <Paper className={classes.root} elevation={1}>
          <InputBase className={classes.input} value={this.state.searchTerm} onChange={this.handleChange.bind(this,'searchTerm')} placeholder="Search For Artists or Albums" o />
          <IconButton className={classes.iconButton} aria-label="Search" type="submit">
            <SearchIcon />
          </IconButton>
          <Divider className={classes.divider} />
          <FormControl component="fieldset" className={classes.formControl}>
            <RadioGroup row
              aria-label="Gender"
              name="gender1"
              className={classes.group}
              value={this.state.filter}
              onChange={this.handleChange.bind(this,'filter')}
            >
              <FormControlLabel value="1" control={<Radio color='secondry' />} label="Artist" />
              <FormControlLabel value="2" control={<Radio color='secondry' />} label="Album" />
            </RadioGroup>
          </FormControl>
        </Paper>
        
        
      </form>
    );
}};

SearchBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchBar);