import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import AudioTrackIcon from '@material-ui/icons/Audiotrack';
import {Link} from 'react-router-dom';

const formatTime = (ms) => {
  let seconds = ms / 1000;
  let minutes = Math.floor( seconds / 60 );
  seconds = Math.floor(seconds - (minutes * 60)); 
  return `${('0' + minutes).substr( -2 )}:${('0' + seconds).substr( -2 )}`;
}

const SongListItem = (props) => {
  const { data,listClass } = props;
  return (<Link to={`/album/${data.collectionId}`}><ListItem className={listClass}>
        <ListItemAvatar>
          <Avatar>
            <AudioTrackIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={data.trackName}
          secondary={formatTime(data.trackTimeMillis)}
        />
      </ListItem></Link>)
};

export default SongListItem;