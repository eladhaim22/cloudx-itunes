import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import reducers from '../reducers/index';


const createStoreWithMiddleware = compose(
    applyMiddleware(reduxThunk))(createStore);

const store = createStoreWithMiddleware(reducers);

export default store;