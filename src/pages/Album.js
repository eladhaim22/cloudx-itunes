import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GenericList from '../components/GenericList.js';
import SongListItem from '../components/SongListItem.js';
import {axios} from '../actions/axiosWrapper';

class Album extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      albumName:undefined,
      songs: [],
      error:undefined
    }
  }

  componentWillMount(){
    let queryString = new URLSearchParams([["id",this.props.match.params.id],["entity","song"]]);
    axios.get('/api/lookup?' + queryString).then(response => {
      let albumName = response.data.results[0].collectionName; 
      response.data.results.splice(0,1);
      this.setState({songs:response.data.results,albumName:albumName});
    })
    .catch(error => 
      this.setState({error:error})
    )
  }

  render() {
  	return (
      <div>
        {this.state.albumName ? <h2>{this.state.albumName}</h2> : null}
    		{this.state.songs ? 
            
    		   	<GenericList items={this.state.songs} itemComponent={<SongListItem/>}/>
        : null}
      </div>
    )}
}

export default Album;

