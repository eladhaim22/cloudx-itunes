import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SearchBar from '../components/SearchBar.js';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import GenericList from '../components/GenericList.js';
import AlbumListItem from '../components/AlbumListItem.js';
import {axios} from '../actions/axiosWrapper';

class Artist extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      artistName:undefined,
      albums: [],
      error:undefined
    }
  }

  componentWillMount(){
    let queryString = new URLSearchParams([["id",this.props.match.params.id],["entity","album"],["attribute","album"]]);
    axios.get('/api/lookup?' + queryString).then(response => {
      let artistName = response.data.results[0].artistName; 
      response.data.results.splice(0,1);
      this.setState({albums:response.data.results,artistName:artistName});
    })
    .catch(error => 
      this.setState({error:error})
    )
  }

  render() {
  	return (
      <div>
        {this.state.artistName ? <h2>{`${this.state.artistName} Album's`}</h2> : null}
        {this.state.albums ? 
          <GenericList items={this.state.albums} itemComponent={<AlbumListItem/>}/>
        : null}
      </div>
    )}
}



export default Artist;

