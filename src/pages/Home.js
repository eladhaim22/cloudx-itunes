import React, { Component } from 'react';
import GenericList from '../components/GenericList.js';
import { connect } from 'react-redux';
import { search } from '../actions/searchActions';
import AlbumListItem from '../components/AlbumListItem.js';
import ArtistListItem from '../components/ArtistListItem.js';

class Main extends Component {
  
  constructor(props) {
    super(props);
  }

  render() {
  	return (
          this.props.results ?
            <div>
              <h2>{this.props.type == 1 ? 'Artists' : 'Albums'}</h2>
      		   	<GenericList 
             		items={this.props.results} 
             		itemComponent={this.props.type == 1 ? <ArtistListItem/> : <AlbumListItem/>}/>
            </div>
          :null 
    )}
}

Main = connect(
    state => {
        return {
            results: state.search.results,
            error: state.search.error
        }
    }, {search}
)(Main);

export default Main;

