import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SearchBar from '../components/SearchBar.js';
import Home from './Home.js';
import Album from './Album.js';
import Artist from './Artist.js';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { search } from '../actions/searchActions';
import Loader from '../components/Loader.js';
import {Route, Switch} from 'react-router-dom';

const styles = theme => ({
  root: {
  	textAlign:'center'
  },
  title: {
    margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
  },
  searchBar:{
  	marginTop:'10px',
  	display:'inline-block',
  	width:'50%'
  },  
  divRoot:{
  	textAlign:'center',
  	color:'white'
  },
});



class Main extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
    	type:1
    }
  }

  handleSubmit = (term,filter) => {
  	this.props.search(term,filter);
  	this.setState({type:filter});
  	this.props.history.push('');
  }

  render() {
  	const { classes } = this.props;
    return (
    	<div className={classes.root}>
    		<SearchBar formClass={classes.searchBar} onSubmit={this.handleSubmit.bind(this)}/>
    		<div className={classes.divRoot}>
    		{this.props.loading ? <Loader/> : null}  
	    		<Switch>
	    			<Route exact path="/" render={(props) => <Home {...props} type={this.state.type}/>}/>
	    			<Route exact path="/album/:id" component={Album} />
	          		<Route exact path="/artist/:id/albums" component={Artist} />}
	    		</Switch>	
    		</div>
    	</div>
    )}
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
};

Main = connect(
    state => {
        return {
            results: state.search.results,
            error: state.search.error,
            loading: state.loader.loading
        }
    }, {search}
)(Main);

export default withStyles(styles)(Main);

