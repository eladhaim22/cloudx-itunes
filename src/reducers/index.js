import { combineReducers } from 'redux';
import searchReducer from './searchReducer';
import loaderReducer from './loaderReducer';

const rootReducer = combineReducers({
    search: searchReducer,
    loader: loaderReducer
});

export default rootReducer;
