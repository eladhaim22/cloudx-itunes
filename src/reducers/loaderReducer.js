import { ENABLE_LOADER, DISABLE_LOADER } from '../actions/types';

const INITIAL_STATE = {loading:false };

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case ENABLE_LOADER:
    		return {...state, loading:true};
    	case DISABLE_LOADER:	
    		return {...state, loading:false};
    }
    return state;
}