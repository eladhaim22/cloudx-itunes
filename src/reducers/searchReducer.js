import { SEARCH_SUCCESS, SEARCH_ERROR } from '../actions/types';

const INITIAL_STATE = {results:undefined,error:undefined,loading:false };

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case SEARCH_SUCCESS:
            return {...state, results: action.payload.results,error:undefined};

        case SEARCH_ERROR:
            return {...state, results:undefined, error:action.payload};
    }
    return state;
}
